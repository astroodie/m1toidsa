
F90 = gfortran -fPIC -fdefault-double-8 -fdefault-integer-8 -fdefault-real-8 

#F90FLAGS = -O2 -ffpe-trap=invalid,zero,overflow,underflow
F90FLAGS = -O -Wall -fcheck=all -g -fbacktrace

MYHOME=/Users/pan/codes
HDF5HOME=/Applications/mesasdk

EOSHOME=$(MYHOME)/eosdriverpy
HDF5INCS=-I$(HDF5HOME)/include
HDF5LIBS=-L$(HDF5HOME)/lib -lhdf5 -lhdf5_fortran -lhdf5 -lz

SOURCES=idsa_units_module.F90 idsa_species_module.F90 model_module.F90 idsa_module.F90
OBJECTS=$(SOURCES:.F90=.o)
EXTRADEPS=

all: copy main

copy:
	cp $(EOSHOME)/nuc_eos.a $(EOSHOME)/eosmodule.mod ./

$(OBJECTS): %.o: %.F90 $(EXTRADEPS)
	$(F90) $(F90FLAGS) $(DEFS) $(MODINC) -c $< -o $@
	
main: $(OBJECTS) main.F90
	$(F90) $(F90FLAGS) -o main main.F90 $(SOURCES) nuc_eos.a $(HDF5LIBS)

clean: 
	rm -rf *.o *.mod main
