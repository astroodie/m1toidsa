import numpy as np
import matplotlib.pyplot as plt

#name = "s20_SFHo_GREP"
#fn   = "debug_GREP.dat"

name = "s20_SFHo_NW"
fn   = "debug_NW.dat"

db = np.loadtxt(fn,unpack=True)
plt.plot(1)
plt.plot(db[0],db[1],'k-',label="f")
plt.plot(db[0],db[2],'k--')
plt.plot(db[0],db[3],'r-',label="ft")
plt.plot(db[0],db[4],'r--')
#plt.plot(db[0],db[2],'b-',label="fs")
plt.title(name)
plt.ylim([1.e-5,1.0])
plt.xlim([1.e5,1.e8])
plt.xscale('log')
plt.yscale('log')
plt.legend(loc='best')
plt.xlabel('Radius (cm)')
plt.ylabel(r'$Y_\nu$')
plt.tight_layout()
plt.savefig('fig_'+name+'.png')
